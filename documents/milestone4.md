# Meilenstein 4

Der dritte Teil der Projektarbeit behandelte insbesondere den Einsatz von künstlicher Intelligenz und die Interaktion
des Spielers mit der Game-World. Aus Eigeninteresse wurde auch die Thematik des _Testen von Games_ etwas vertieft.

## Auftrag

Dabei sollten folgende Elemente sinnvoll in das eingene Spiel integriert werden

- Erweitere dein Spiel so, dass sich die NPCs autonom in der Welt bewegen
- Mach die Bewegungen und Handlungen der NPCs abhängig von den Aktionen des Spielers
- Integriere mindestens 2 verschiedene Reaktionsmuster auf Handlungen des Spielers Beispiel: Ein Gegner greift an, wenn
  auf ihn geschossen wird oder man sich ihm nähert, ein anderer ergreift die Flucht.

### Fähigkeiten

Wobei so verschiedene Fähigkeiten praktisch erlangt werden sollen.

- Ich kann nicht vom Spieler gesteuerte Elemente mit künstlicher Intelligenz ausstatten
- Ich kann die Umgebung abhängig von den Handlungen des Spielers auf diesen reagieren lassen

## Implementierung im Projekt

### Künstliche Intelligenz

Durch den Bau einer _Holzfällerhütte_ begibt sich nun ein Siedler selbstständig auf den Weg zu seiner Hütte. Dafür wird
von dem Hauptquartier bis zu seiner Hütte eine Adjazenzmatrix erzeugt. In einer zu wählenden Granularität werden so
Koordinaten generiert, welche Wegpunkte für die Pfadfindung repräsentieren. Mittels der Matrix und des
Dijkstra-Algorithmus wird nun der kürzeste Pfad eruiert und der Siedler kann seine Reise antreten.

### Reaktion

Sobald eine _Holzfällerhütte_ von einem Siedler besetzt wird, beginnt der Holz-Vorrat stetig zu wachsen. Wobei für den
Bau seiner Hütte jedoch auch bereits eine bestimmte Anzahl Holz verbraucht wurde.

Wurde einmal genügend Holz erwirtschaftet, erscheint eine Meldung, dass die Partie gewonnen sei.

![Reaktion: Die Siegesbedingung wurde erreicht](slides/dist/assets/victory.png)

## Architektur

Die Architektur konnte noch bereinigt werden um die Abhängigkeiten zwischen den verschiedenen _Services_ (_Controller_)
zu optimieren (siehe Kontextdiagramm unten). Gegenüber der geplanten Architektur[^readme] gibt es einige Abweichungen,
der Grundsatz ist dennoch ähnlich geblieben.

![Architektur-Übersicht](slides/dist/assets/overview.svg)

[^readme]: siehe Klassendiagramm im [Readme](https://gitlab.com/ffhs-sterchi/21hs-GameDev/-/blob/main/README.md)
