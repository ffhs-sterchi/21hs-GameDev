# Meilenstein 3

Der dritte Teil der Projektarbeit behandelte insbesondere das Zusammenspiel zwischen den verschiedenen Game-Objekten.

## Auftrag

Dabei sollten folgende Elemente sinnvoll in das eingene Spiel integriert werden

- Reichere die Umgebung mit Hindernissen an, mit denen die Spielfiguren kollidieren
- Reichere die Umgebung mit Elementen an, mit denen die Spielfigur interagieren kann
  (Items zum Aufnehmen, Schalter zum Betätigen)
- Erweitere das Bewegungsrepertoire deiner Figur mit neuen Aktionen, z.B. Springen, Schiessen, Schieben

### Fähigkeiten

Wobei so verschiedene Fähigkeiten praktisch erlangt werden sollen.

- Ich kann verschiedene physikalische Eigenschaften in mein Spiel integrieren
- Ich kann die Kollisionen zwischen verschiedenen 3D Elementen erkennen und das Spiel darauf reagieren lassen
- Ich kann Figuren untereinander, mit Elementen und der Umgebung interagieren lassen
- Ich kann Elemente auf physikalische Aktionen reagieren lassen

## Implementierung im Projekt

### Interaktion

Es können Häuser gebaut werden, indem im Overlay Menu der entsprechende Button verwendet wird.
Dabei erscheint ein Haus an der Position des Cursors und kann an einer entsprechenden Stelle plaziert werden.
Sollte das noch nicht gebaute Haus mit einem anderen Objekt kolidieren, kann das Haus nicht gebaut werden.
Das wird dem User mit einem roten Schimmer om das Haus vermittelt, das ist auf dem folgenden Bild zu erkennen.

Ein gebautes Haus kann anschliessend auch wieder entfernt werden, dass mit dem entsprechenden Button im Menu.

![Funktion: Haus bauen](slides/dist/assets/bauen.png)

Das Tracken des Mauszeigers ist somit eine neue Art mit dem Spiel zu interagieren. 

### Kollision

Jedes Objekt in der Spielwelt ist im [`GameState`](../settlers/src/services/game-state.ts) registriert. 
Dadurch kann der [`ConstructionHandler`](../settlers/src/services/construction-handler.ts), welcher für das Bauen zuständig ist prüfen, ob das neue Haus mit einem anderen Objekt kollidieren würde.

Die Kollisionsprüfung ermittelt für jedes Objekt, ob dessen Bounding-Sphere mit der des zu bauenden Hauses eine Überschneidung hat. Darin ist allerdings noch ein Fehler enthalten, so wird nicht die Bounding-Sphere des neuen Objektes geprüft, sondern nur der Mittelpunkt - siehe [ConstructionHandler::hasCollision](https://gitlab.com/ffhs-sterchi/21hs-GameDev/-/blob/main/settlers/src/services/construction-handler.ts#L66).

