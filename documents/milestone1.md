# The Lumberjacks - Web Edition

## Inspiration: Siedler II
Der Aufbaustrategieklassiker Siedler II [^Siedler] im Jahre 1996, insbesondere durch sein komplexes und fesselndes GamePlay. Um die Grundkonzepte der Game-Entwicklung zu erlernen und praktisch zu festigen, werden einige der Kernelemente in einer modernen Game-Engine neu implementiert.
Eine ausführliche Beschreibung der originalen Spielmechaniken finden sich im offiziellen [Handbuch](misc/Die_Siedler_2_Handbuch.pdf).

## Spielregeln

In diesem Projekt sollen nicht annähernd alle Konzepte umgesetzt werden, sondern eine minimal spielbare Version umgesetzt werden. Wie das Spiel funktioniert, wird nachfolgend kurz erläutert und bei den [Anforderungen](#anforderungen) noch genauer spezifiziert.

## Aufbau

Eine an das europäische Mittelalter erinnernde Siedlung, soll aufgebaut werden. Ausgangspunkt ist dabei ein **Hauptquartier**, welches Platz für die ersten **Siedler** und deren **Vorräte** dient.
Vom Stützpunkt ausgehend sollen weitere Gebäude errichtet werden können. So wird eine **Holzfällerhütte** für die Beschaffung des wichtigen Rohstoffes **Holz** zuständig sein. Ein Jäger wird von seiner **Jagdhütte** aus für die **Nahrung**sbeschaffung zuständig sein, ohne welche die Siedler natürlich verhungern würden.
Um die beiden Rohstoffen (Holz & Nahrung) zurück zum Hauptquartier bringen zu können, bedarf es effizienter Transportwege, auf welchen eifrige Siedler die produzierten Waren fortbewegen.

## Anforderungen
### Funktionale Anforderungen

|#|Erfüllung|Erfüllt|Titel|Beschreibung|
|---|---|---|---|---|
|1|muss|Ja  |Spielwelt|Eine flache, mit Texturen versehene Landschaft, ist aus der Vogelperspektive ersichtlich. Der dabei sichtbare Ausschnitt der Spielwelt ist mittels Tastatur-Pfeiltasten zu verändern. Die zu erkundende Welt ist dabei endlich, wodurch der Spieler den Ausschnitt nicht beliebig weit verschieben kann.|
|2|muss|Ja  |Hauptquartier|Zu Beginn des Spiels soll in der Spielwelt bereits das Hauptquartier stehen, welches vorerst nur einen ästhetischen Beitrag zum Spiel bringt.|
|3|muss|Ja  |Lager|Die vorhandene Menge an Nahrung und Holz soll in einem klassischen HUD[^HUD] ersichtlich sein. Initial soll ein Grundvorrat vorhanden sein, welcher sich erst mit den Produktionsstätten verändern wird, somit aktuell noch statisch ist.|
|4|muss|Teils|Flora & Fauna|Die Spielwelt ist mit Bäumen und Wildtieren angereichert. Dabei müssen sich Bäume und Tiere noch nicht reproduzieren können. Die Tiere sollen sich allerdings in einem definierten Sektor frei und autonom bewegen können. |
|5|muss|Ja  |Bauen|Über das HUD[^HUD] soll ein generisches Haus zum bauen ausgewählt werden können. Wo das Haus gebaut werden kann wird durch Bauplätze, also Markierungen auf der Landschaft, vorgegeben. Gebaut wird das Gebäude mit einem Klick auf den entsprechenden Bauplatz. Weiter kann auch über das HUD eine Art "Abriss-Werkzeug" ausgewählt werden, mit welchem durch einen Klick auf ein Gebäude, dieses wieder entfernt wird. Das Bauen eines Hauses zieht eine zu bestimmende Anzahl Holz aus dem Lager ab, das Abreissen erhöht diese Menge wieder um die Hälfte der Baukosten.|
|6|muss|Ja  |Holzfällerhütte|Mit der Bauen-Funktion soll eine Holzfällerhütte gebaut werden können. Einmal gebaut, begibt sich ein Holzfäller vom Hauptquartier ausgehend Richtung Holzfällerhütte und verschwindet darin. Vorerst wird noch keine Animation für das effektive Arbeiten implementiert. Sobald ein Holzfäller im Gebäude ist, wird in einem zu definierenden Intervall das Holz im Lager erhöht.|
|7|muss|Ja  |Siegesbedingung|Um eine Partie zu gewinnen, muss eine zu definierende Menge an zu bestimmenden Rohstoffen erwirtschaftet werden. Bei Erreichung der Bedingung, soll eine Meldung über den Sieg angezeigt werden, das Spiel pausiert werden und ein Button zum _Restart_ des Spiels angezeigt werden.|
|8|kann|Nein|Jagdhütte|Mit der Bauen-Funktion soll eine Jagdhütte gebaut werden können. Einmal gebaut, begibt sich ein Jäger vom Hauptquartier ausgehend Richtung Jagdhütte und verschwindet darin. Vorerst wird noch keine Animation für das effektive Arbeiten implementiert. Sobald ein Jäger im Gebäude ist, wird in einem zu definierenden Intervall die Nahrung im Lager erhöht.|
|9|kann|Nein|Transport|Um die Warenwirtschaft etwas realistischer zu gestalten, sollen Wege zwischen den Gebäude gebaut werden können, auf welchen sich Siedler befinden um so die Waren von einer Produktionsstätte zum Hauptquartier zu befördern. Produktionsstätten würde also nicht mehr in einem Intervall das Lager erhöhen, sondern die Waren bei sich für die Abholung bereitstellen, erst wenn die Ware im Hauptquartier angekommen ist, wir das Lager erhöht.|
|10|kann|Nein|Bevölkerung|Die Bevölkerung soll begrenzt werden und mit dem Bau von Wohnhäusern erhöht werden. Je Einwohner wird eine zu definierende Menge an Nahrung, in einem bestimmten Invervall, verbraucht. Wenn nicht genügend _arbeitslose_ Siedler vorhanden sind, wird eine Produktionsstätte nicht besetzt und generiert somit auch keine Waren. Wenn die Nahrung unter Null sinken würde, verhungert ein zufälliger Siedler und minimiert die maximale Bevölkerungszahl temporär. |

### Nicht funktionale Anforderungen

- Das Spiel soll im Chrome-Browser mit Version 90 oder höher lauffähig sein

### Allgemeine Anforderungen
Durch die Aufgabestellungen zur Semesterarbeit auf Moodle können folgende allgemeinen Anforderungen an das Projekt entnommen werden. Diese Ziele sollen gem. Dozent, mehr als Leitfragen dienen und stellen keine zwingend einzuhaltende Anforderungen dar.
#### Organisatorisches (Teil 1)
- Ich kann Anforderungen für ein konkretes Projekt zur Spielentwicklung definieren und diese einordnen.
- Ich kann ein Projekt zur Spielentwicklung strukturieren und eine Roadmap definieren.
- Ich kann die Design-Grundbausteine eines Spiels definieren. 
- Ich kann passende Tools und Technologien zur Entwicklung von Spielen definieren und bewerten. 
#### 3D (Teil 2)
- Ich kann 3D Grafiken in mein Spiel integrieren
- Ich kann die 3D Elemente in meinem Spiel bewegen und anhand von Benutzereingaben steuern
- Ich kann 3D Elementen, welche nicht vom Spieler gesteuert werden, eigene Bewegungsmuster zuteilen
#### Physik & Interaktion (Teil 3)
- Ich kann verschiedene physikalische Eigenschaften in mein Spiel integrieren
- Ich kann die Kollisionen zwischen verschiedenen 3D Elementen erkennen und das Spiel darauf reagieren lassen
- Ich kann Figuren untereinander, mit Elementen und der Umgebung interagieren lassen
- Ich kann Elemente auf physikalische Aktionen reagieren lassen
#### KI (Teil 4)
- Ich kann nicht vom Spieler gesteuerte Elemente mit künstlicher Intelligenz ausstatten
- Ich kann die Umgebung abhängig von den Handlungen des Spielers auf diesen reagieren lassen
#### Präsentation (Teil 5)
- Ich kann ein Projekt zur Umsetzung von Spielen vor einem Publikum vorstellen und kann mich dabei auf die Kernaspekte fokussieren
- Ich bin in der Lage meine Erkenntnisse und mein Wissen aus einem Projekt anderen zur Verfügung stellen

## Projektstruktur

Die Anforderungen an das Projekt werden iterativ umgesetzt und im Sinne des Agile-Manifesto[^agile] bei Bedarf angepasst. Die eingeplanten Arbeiten werden als [Gitlab-Issues](https://gitlab.com/ffhs-sterchi/21hs-GameDev/-/issues) getrackt.

## Design

Folgendes Diagram soll als grobe Design-Vorlage dienen.

```plantuml
@startuml
skinparam shadowing false
skinparam classBackgroundColor white
skinparam classBorderColor #464646

class GameController {
    -List updatableObjects
    -List renderLayers
    -GameState state
    void loop()
}
class GameWorld implements RenderLayer
class GameHUD implements RenderLayer
interface RenderLayer {
   List drawableObjects
   void render()
}
interface Updatable {
    void update(long time)
}
interface UpdatableDrawable implements Updatable, Drawable {
    void update(long time)
    void draw()
}
interface Drawable {
    void draw()
}
class GameState implements UpdatableDrawable {
    bool isVictoryConditionReached()
    bool isPaused()
    long inGameTime()
}

GameController --> Updatable
GameController --> Drawable
GameController -> GameWorld
GameHUD <- GameController
GameController --o GameState
Stock <- GameState
Population <- GameState
GameTime <- GameState

class GameTime implements Updatable
class Stock implements Updatable
class Population implements Updatable

abstract Building implements UpdatableDrawable
class Headquarter extends Building
class LumberjackHut extends Building
class HuntingLodge extends Building

abstract Creature implements UpdatableDrawable
class Settler extends Creature
class Deer extends Creature

@enduml
```

## Tooling

Das Spiel wird als Web-Applikation implementiert und somit mit JavaScript / TypeScript geschrieben werden. Der Einsatz eines zu verwendenden Frameworks, wird bei der Implementierung noch festgelegt. Dabei werden insbesondere die beiden Tools _Phaser_[^phaser] und _Babylon.js_[^babylon] noch näher angeschaut.

## Assets

Es gibt verschiedene Quellen um 3D-Modelle zu erhalten - Beispiel: https://www.turbosquid.com/Search/3D-Models/free/medieveal?file_type=191,197,119

[^HUD]: heads-up display
[^agile]: https://agilemanifesto.org/
[^phaser]: https://phaser.io/
[^babylon]: https://www.babylonjs.com/
[^Siedler]: https://de.wikipedia.org/wiki/Die_Siedler_II_%E2%80%93_Veni,_Vidi,_Vici) [glänzte](https://archive.org/details/powerplaymagazine-1996-06/page/120/mode/1up
