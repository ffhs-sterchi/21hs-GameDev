# 21hs GameDev

### [Zum Game!](https://ffhs-sterchi.gitlab.io/21hs-GameDev/)
#### [Präsentation](https://ffhs-sterchi.gitlab.io/21hs-GameDev/slides)
#### [Bericht: Meilenstein 1](documents/milestone1.md)
#### [Bericht: Meilenstein 2](documents/milestone2.md)
#### [Bericht: Meilenstein 3](documents/milestone3.md)
#### [Bericht: Meilenstein 4](documents/milestone4.md)
