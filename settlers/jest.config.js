/** @type {import('ts-jest/dist/types').InitialOptionsTsJest} */
module.exports = {
    rootDir: 'src',
    preset: 'ts-jest/presets/js-with-ts',
    transformIgnorePatterns: [],
    testEnvironment: 'node',
    globals: {
        'ts-jest': {
            tsconfig: {
                allowJs: true,
                checkJs: false,
            },
            isolatedModules: true,
        },
    },
};
