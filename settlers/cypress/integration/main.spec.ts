interface SettlersMessage {
    type: string;
    data?: any;
}

describe('Settlers', () => {
    let messageSpy: Cypress.Omit<sinon.SinonSpy, 'withArgs'> &
        Cypress.SinonSpyAgent<sinon.SinonSpy> &
        sinon.SinonSpy;
    beforeEach(() => {
        cy.visit('/?debug&gameSpeed=1000', {
            onBeforeLoad(win) {
                cy.stub(win.console, 'log').as('consoleLog');
                cy.stub(win.console, 'error').as('consoleError');
                messageSpy = cy.spy(win, 'postMessage').as('postMessage');
            },
        });
    });
    afterEach(() => {
        cy.get('@consoleError').should('not.have.been.called');
    });

    it('should build Lumberjackhut', { retries: { openMode: 0, runMode: 2 } }, () => {
        assertMessage({ type: 'OnReady' }).then(() => {
            cy.get('canvas').click(250, 610);
            cy.get('canvas').click(500, 500);
        });
        assertMessage({ type: 'NewStock', data: { wood: 5 } });

        assertMessage({ type: 'NewBuilding' });

        assertMessage({
            type: 'BuildingIsProducing',
            data: 'Holzfällerhütte',
        });
        assertMessage({ type: 'NewStock', data: { wood: 7 } }, 5000);
    });

    function assertMessage(message: SettlersMessage, timeout: number = 20000) {
        const matchers = Object.keys(message).reduce((acc, key) => {
            let value = message[key];
            if (key === 'type') {
                value = `settlers${value}`;
            }
            return acc.and(Cypress.sinon.match.has(key, value));
        }, Cypress.sinon.match.has('type'));
        return cy.get('@postMessage', { timeout }).should('have.been.calledWithMatch', matchers);
    }

    // function assertMessage(message: SettlersMessage, timeout: number = 4000) {
    //     const matchers = Object.keys(message).map(key => {
    //         return Cypress.sinon.match
    //             .has(key, message[key])
    //             .or(Cypress.sinon.match.has(key, `settlers${message[key]}`));
    //     });
    //     const spy: string = Math.random().toFixed(2);
    //     messageSpy.withArgs(message).as(spy);
    //     return cy.get(`@${spy}`, { timeout }).should('have.been.calledWithMatch', ...matchers);
    // }
});
