import { ArcRotateCamera, Vector3 } from "@babylonjs/core";

/**
 * TODO: describe
 */
export class Camera {
    public static get default() {
        return new ArcRotateCamera(
            "Camera",
            -7.3, // rotation
            0.9, // azimut
            20, // flight height
            Vector3.Zero(),
            null
        );
    }

    public static get flat() {
        return new ArcRotateCamera(
            "CameraFlat",
            0, // rotation
            0, // azimut
            30, // flight height
            Vector3.Zero(),
            null
        );
    }

    constructor(canvas: HTMLCanvasElement, camera: ArcRotateCamera) {
        camera.attachControl(canvas, true);
        camera.upperBetaLimit = Math.PI / 2.2;
        camera.upperRadiusLimit = 50;
        camera.lowerRadiusLimit = 5;
        camera.useFramingBehavior = true;
    }
}
