import { Injectable } from '../injection/injectable';
import { Engine, Scene } from '@babylonjs/core';
import { Camera } from './camera';

/**
 * TODO: describe
 */
export class SceneWrapper extends Injectable {
    public scene: Scene;
    public engine: Engine;
    private canvas: HTMLCanvasElement;
    private camera: Camera;

    constructor() {
        super('sceneWrapper', SceneWrapper.prototype);
        this.init();
    }

    protected init() {
        this.canvas = SceneWrapper.getCanvas();
        this.engine = new Engine(this.canvas, true);
        this.scene = new Scene(this.engine);
        this.camera = new Camera(this.canvas, Camera.default);
    }

    private static getCanvas(): HTMLCanvasElement {
        return document.getElementById('renderCanvas') as HTMLCanvasElement;
    }
}
