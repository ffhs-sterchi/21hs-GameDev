import '@babylonjs/inspector';
import { Menu } from './userinterface/menu';
import { MeshLoader } from './services/mesh/mesh-loader';
import { GameState } from './services/game/state/game-state';
import { GameHandler } from './services/game/game-handler';
import { Injector } from './injection/dependency-injector';
import { SceneWrapper } from './engine/scene-wrapper';
import { WorldService } from './services/world-service';
import { TestMediator } from './services/test-mediator';
import { LocationHandler, Param } from './services/location-handler';
import { DrawableFactory } from './services/game/drawable-factory';
import { ConstructionHandler } from './services/game/construction-handler';

/**
 * Is responsible to wire up the Application,
 * including providing Providers for the Dependency-Injection
 * @see DependencyInjector
 */
class App {
    constructor() {
        Injector.provide(new LocationHandler());
        Injector.provide(new GameState());
        Injector.provide(new SceneWrapper());
        Injector.provide(new WorldService());
        Injector.provide(new MeshLoader());
        Injector.provide(new DrawableFactory());

        Injector.provide(new ConstructionHandler());
        Injector.provide(new GameHandler());
        Injector.provide(new Menu());

        Injector.gameHandler.start();

        if (Injector.locationHandler.hasParam(Param.debug)) {
            Injector.provide(new TestMediator());
            this.setupInspector();
        }

        this.initMainRenderLoop();
    }

    /**
     * activates the babylonJs Render-Loop
     * see: https://doc.babylonjs.com/start/chap1/first_app
     * @private
     */
    private initMainRenderLoop() {
        Injector.sceneWrapper.engine.runRenderLoop(() => {
            Injector.sceneWrapper.scene.render();
        });
    }

    /**
     * hides / shows the BabylonJS-Inspector on CRTL + SHIFT + ALT + I
     * see: https://doc.babylonjs.com/toolsAndResources/tools/inspector
     * @private
     */
    private setupInspector() {
        window.addEventListener('keydown', event => {
            const shiftCtrlAltI =
                event.shiftKey && event.ctrlKey && event.altKey && event.keyCode === 73;
            if (shiftCtrlAltI) {
                if (Injector.sceneWrapper.scene.debugLayer.isVisible()) {
                    Injector.sceneWrapper.scene.debugLayer.hide();
                } else {
                    Injector.sceneWrapper.scene.debugLayer.show();
                }
            }
        });
    }
}

const newLocation = new LocationHandler();
// window.location = new LocationManager();
new App();
