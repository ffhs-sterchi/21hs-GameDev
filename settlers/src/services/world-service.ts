import { Injectable } from '../injection/injectable';
import { Landscape } from '../drawables/environment/landscape';
import { Light } from '../drawables/environment/light';
import { Sky } from '../drawables/environment/sky';

/**
 * TODO: describe
 */
export class WorldService extends Injectable {
    public readonly ground: Landscape = new Landscape();
    private readonly light: Light;
    private readonly sky: Sky;

    constructor() {
        super('worldService', WorldService.prototype, ['sceneWrapper']);
        this.light = new Light(this.ground);
        this.sky = new Sky();
    }
}
