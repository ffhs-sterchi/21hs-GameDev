import { Injectable } from '../injection/injectable';

/**
 * Possible Query-Params to provide in the URL
 */
export const Param = {
    debug: 'debug',
    speed: 'gameSpeed',
};

/**
 * Helper to get and set values provided in the URL
 */
export class LocationHandler extends Injectable {
    get url(): URL {
        return new URL(window.location.href);
    }

    constructor() {
        super('locationHandler', LocationHandler.prototype);
    }

    setParam(key: string, value: string | number | boolean) {
        this.url.searchParams.set(key, JSON.stringify(value));
        window.history.pushState({}, '', this.url.toString());
    }

    getParam(key: string) {
        return this.url.searchParams.get(key) || '';
    }

    hasAtLeastOneParamOf(...keys: string[]): boolean {
        for (const key of keys) {
            if (this.hasParam(key)) return true;
        }
        return false;
    }

    hasParam(key: string): boolean {
        return this.url.searchParams.has(key);
    }

    getIntParam(key: string) {
        return parseInt(this.getParam(key), 10);
    }
}
