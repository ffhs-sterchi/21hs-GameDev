import { Injectable } from '../injection/injectable';
import { debounceTime, skipUntil, Subject, take } from 'rxjs';
import { Drawable } from '../drawables/drawable';
import { Injector } from '../injection/dependency-injector';

/**
 * TODO: describe
 */
export class TestMediator extends Injectable {
    constructor() {
        super('testMediator', TestMediator.prototype, ['gameState']);
        this.logSettlersMessages();
        this.publishOnReady();
        this.publishNewBuilding();
        this.publishNewStock();
    }

    private publishNewStock() {
        Injector.gameState.inventory.stock$.subscribe(stock => {
            this.postMessage('NewStock', stock);
        });
    }

    private publishNewBuilding() {
        Injector.gameState.building.subscribe(building => {
            this.postMessage('NewBuilding', building.representation.name);
            building.isProducing.subscribe(isProducing => {
                if (isProducing)
                    this.postMessage('BuildingIsProducing', building.representation.name);
            });
        });
    }

    private logSettlersMessages() {
        window.addEventListener('message', message => {
            if (message.data?.type?.startsWith && message.data.type.startsWith('settlers')) {
                console.log(message.data);
            }
        });
    }

    private publishOnReady() {
        const rendered = new Subject<Drawable>();
        Injector.gameState.drawable.subscribe(building => {
            building.mesh.onAfterRenderObservable.addOnce(() => {
                rendered.next(building);
            });
        });
        rendered
            .pipe(skipUntil(Injector.gameState.gameIsReady), debounceTime(100), take(1))
            .subscribe(() => this.postMessage('OnReady'));
    }

    private postMessage(messageName: string, data?: any) {
        window.postMessage({
            type: `settlers${messageName}`,
            data,
        });
    }
}
