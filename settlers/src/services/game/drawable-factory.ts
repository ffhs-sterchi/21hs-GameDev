import { Injectable } from '../../injection/injectable';
import { Injector } from '../../injection/dependency-injector';
import { Drawable } from '../../drawables/drawable';
import { LumberjackHut } from '../../drawables/buildings/lumberjackHut';
import { Headquarter } from '../../drawables/buildings/headquarter';
import { Tree } from '../../drawables/tree';
import { Settler } from '../../drawables/settler';

/**
 * TODO: describe
 */
export class DrawableFactory extends Injectable {
    private readonly factories = new Map<Drawable, () => Drawable>([
        [LumberjackHut.prototype, () => new LumberjackHut()],
        [Headquarter.prototype, () => new Headquarter()],
        [Tree.prototype, () => new Tree(0, 0)],
        [Settler.prototype, () => new Settler()],
    ]);

    constructor() {
        super('drawableFactory', DrawableFactory.prototype);
    }

    createDrawable<T extends Drawable>(type: T): T {
        const factory = this.factories.get(type);

        if (factory == null) {
            throw Error('DrawableType not supported! Please add it to `DrawableFactory.factories`');
        }

        const drawable = factory();
        Injector.gameState.addDrawable(drawable);
        return drawable as T;
    }
}
