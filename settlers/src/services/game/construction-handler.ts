import { Building } from '../../drawables/buildings/building';
import {
    Color3,
    HighlightLayer,
    PointerDragBehavior,
    PointerEventTypes,
    Vector3,
} from '@babylonjs/core';
import { Injector } from '../../injection/dependency-injector';
import { Injectable } from '../../injection/injectable';

export class ConstructionHandler extends Injectable {
    private readonly highlightLayer = new HighlightLayer(
        'buildingHighlighter',
        Injector.sceneWrapper.scene,
    );
    private readonly pointerDragBehavior: PointerDragBehavior = new PointerDragBehavior({
        dragPlaneNormal: new Vector3(0, 1, 0),
    });

    constructor() {
        super('constructionHandler', ConstructionHandler.prototype, [
            'drawableFactory',
            'gameState',
            'sceneWrapper',
        ]);
    }

    startBuilding(buildingType: Building) {
        const building = Injector.drawableFactory.createDrawable(buildingType);
        const collisionHandler = this.pointerDragBehavior.onDragObservable.add(() => {
            this.handleCollision(building);
        });

        this.pointerDragBehavior.onDragEndObservable.addOnce(() => {
            this.pointerDragBehavior.detach();
            this.pointerDragBehavior.onDragObservable.remove(collisionHandler);

            if (this.hasCollision(building)) {
                Injector.gameState.removeBuilding(building);
            } else {
                this.handleCollision(building);
                Injector.gameState.inventory.remove(building.costs);
                Injector.gameState.doneBuilding(building);
            }
        });
        building.mesh.addBehavior(this.pointerDragBehavior);
        this.pointerDragBehavior.startDrag();
    }

    startWrecking() {
        const observable = Injector.sceneWrapper.scene.onPointerObservable.add(pointerInfo => {
            switch (pointerInfo.type) {
                case PointerEventTypes.POINTERDOWN:
                    const building = Building.at(pointerInfo);
                    if (building && !building.hasName('Hauptquartier')) {
                        Injector.gameState.removeBuilding(building);
                    }
                    Injector.sceneWrapper.scene.onPointerObservable.remove(observable);
                    Injector.sceneWrapper.scene.onPointerMove = null;
                    break;
            }
        });
    }

    private hasCollision(building: Building) {
        const intersectingMeshes = Injector.gameState.drawables
            .filter(d => d !== building)
            .filter(drawable => {
                return drawable.mesh
                    .getBoundingInfo()
                    .boundingSphere.intersectsPoint(building.mesh.position);
            });
        return intersectingMeshes.length > 0;
    }

    private handleCollision(building: Building): void {
        if (this.hasCollision(building)) {
            if (!this.highlightLayer.hasMesh(building.mesh)) {
                this.highlightLayer.addMesh(building.mesh, Color3.Red());
            }
        } else {
            this.highlightLayer.removeMesh(building.mesh);
        }
    }
}
