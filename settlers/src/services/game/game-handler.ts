import { Injector } from '../../injection/dependency-injector';
import { Settler } from '../../drawables/settler';
import { Color3, HighlightLayer, Mesh, Vector3 } from '@babylonjs/core';
import { PathNodeCollection } from '../path-finder';
import { Building } from '../../drawables/buildings/building';
import { interval, Observable, takeUntil } from 'rxjs';
import { Stock } from './state/inventory';
import { Injectable } from '../../injection/injectable';
import { Headquarter } from '../../drawables/buildings/headquarter';
import { Tree } from '../../drawables/tree';
import { GameState } from './state/game-state';
import { Param } from '../location-handler';
import { PathWalker } from '../mesh/path-walker';

/**
 * Possible Configuration for the GameHandler
 */
export class GameHandlerConfig {
    /**
     * The duration of a production-cycle,
     * after which the productionStock is being added to the Inventory
     *
     * @see GameHandler
     * @see productionStock
     */
    readonly productionIntervalMs: number = 5000;

    /**
     * The amount of resources being produced after a production-cycle,
     * by a building producing given resource
     *
     * @see GameHandler
     * @see Building.produces
     */
    readonly productionStock: Stock = {
        wood: 2,
    };

    /**
     * How many units of a given resource is required to win the game
     */
    readonly victoryStock: Partial<Stock> = {
        wood: 200,
    };

    readonly productionInterval: Observable<any> = interval(this.productionIntervalEffectiveMs);

    get productionIntervalEffectiveMs(): number {
        return this.productionIntervalMs * (100 / Injector.gameState.gameSpeed.getValue());
    }

    constructor(config?: Partial<GameHandlerConfig>) {
        if (config) {
            Object.assign(this, config);
        }
    }
}

/**
 * Is responsible to handle game-specific logic like:
 * - if the victory-condition is met
 * - when to produce new resources
 * - what to do, when a building has been constructed
 */
export class GameHandler extends Injectable {
    private readonly state: GameState = Injector.gameState;

    constructor(private readonly config: GameHandlerConfig = new GameHandlerConfig()) {
        super('gameHandler', GameHandler.prototype, ['gameState', 'locationHandler']);
        if (Injector.locationHandler.hasParam(Param.debug)) {
            this.initDebug();
        }
    }

    /**
     * Wire up all the handlers, after the meshes have been loaded
     */
    start(): void {
        Injector.meshLoader.isInitialized.subscribe(() => {
            Injector.drawableFactory.createDrawable(Headquarter.prototype);
            this.createForest();

            this.handleBuilding();
            this.handleProductionInterval();
            this.handleStockChange();

            this.state.gameIsReady.next(true);
        });
    }

    /**
     * Generate some Trees on a given area
     */
    private createForest() {
        Tree.forest(0, 0, 40, 0.02).forEach(position => {
            const tree = Injector.drawableFactory.createDrawable(Tree.prototype);
            tree.mesh.position.x = position.x;
            tree.mesh.position.z = position.y;
        });
    }

    /**
     * When a building has been constructed
     */
    private handleBuilding() {
        this.state.building.subscribe(newBuilding => this.onNewBuilding(newBuilding));
    }

    /**
     * Assigns a Settler to a building and walks the settler to the building
     */
    private onNewBuilding(newBuilding: Building) {
        newBuilding.settler = Injector.drawableFactory.createDrawable(Settler.prototype);
        const settlerMesh = newBuilding.settler.mesh;
        const waypoints = this.generateWaypoints(settlerMesh, newBuilding);

        const highlightLayer = new HighlightLayer('gameHandlerHighlighter', null);
        highlightLayer.addMesh(newBuilding.mesh, Color3.Teal());

        const pathWalker = new PathWalker(waypoints);
        pathWalker.walk(settlerMesh).subscribe(() => {
            newBuilding.isProducing.next(true);
            highlightLayer.dispose();
        });
    }

    private generateWaypoints(dude: Mesh, newBuilding: Building) {
        const grid = PathNodeCollection.gridBetween(dude.position, newBuilding.mesh.position, 0.5);

        const pathStart = { x: dude.position.x, z: dude.position.z };
        const pathEnd = { x: newBuilding.mesh.position.x, z: newBuilding.mesh.position.z };
        return grid
            .findShortestPathNear(pathStart, pathEnd)
            .map(node => new Vector3(node.x, 0, node.z));
    }

    /**
     * When another production cycle is triggered, update the stock
     */
    private handleProductionInterval() {
        this.config.productionInterval.subscribe(v => {
            const currentProductionStock = this.calculateCurrentProductionStock();
            this.state.inventory.add(currentProductionStock);
        });
    }

    /**
     * Calculates the stock produced at the current production cycle
     */
    private calculateCurrentProductionStock(): Partial<Stock> {
        return this.state.buildings
            .filter(building => building.isProducing.getValue())
            .flatMap(building => building.produces)
            .reduce((stock, key) => {
                stock[key] = (stock[key] || 0) + this.config.productionStock[key];
                return stock;
            }, {} as Partial<Stock>);
    }

    /**
     * When the stock changes
     */
    private handleStockChange() {
        this.state.inventory.stock$
            .pipe(takeUntil(this.state.victory))
            .subscribe(stock => this.onNewStock(stock));
    }

    /**
     * Check if victory-condition is met
     */
    private onNewStock(newStock: Stock) {
        // TODO: handle multiple resources...
        if (newStock.wood >= this.config.victoryStock.wood) {
            this.state.victory.next();
        }
    }

    /**
     * Two-way-binds some config parameters to the url
     */
    private initDebug() {
        if (Injector.locationHandler.hasParam(Param.speed)) {
            this.state.gameSpeed.next(Injector.locationHandler.getIntParam(Param.speed));
        }
        this.state.gameSpeed.subscribe(speed =>
            Injector.locationHandler.setParam(Param.speed, speed),
        );
    }
}
