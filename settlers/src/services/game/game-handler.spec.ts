import { GameHandler, GameHandlerConfig } from './game-handler';
import { LumberjackHut } from '../../drawables/buildings/lumberjackHut';
import { MeshLoader } from '../mesh/mesh-loader';
import { Mesh, NullEngine, Scene } from '@babylonjs/core';
import { Injector } from '../../injection/dependency-injector';
import { Subject } from 'rxjs';
import { GameState } from './state/game-state';
import { Stock } from './state/inventory';
import { LoadingMesh } from '../mesh/loading-mesh';
import { LocationHandler } from '../location-handler';

class TestMeshLoader extends MeshLoader {
    constructor(private scene: Scene) {
        super();
    }

    protected override initialize() {
        this.meshes = {
            lumberjackhut: {
                mesh: Mesh.CreateBox('dummy', 1, this.scene),
            } as LoadingMesh,
        };
        this._isInitialized.next(true);
    }
}

class MockLocationHandler extends LocationHandler {
    override hasParam(key: string): boolean {
        return false;
    }
}

describe('GameHandler', () => {
    let sut: GameHandler;
    const scene = new Scene(new NullEngine());
    const productionCycleDo = new Subject<void>();

    beforeEach(() => {
        Object.assign(Injector, {});
        Injector.provide(new MockLocationHandler());
        Injector.provide(new TestMeshLoader(scene));
        Injector.provide(new GameState());
        Injector.provide(
            new GameHandler(
                new GameHandlerConfig({
                    productionInterval: productionCycleDo.asObservable(),
                }),
            ),
        );

        sut = Injector.gameHandler;
    });

    it('should create', () => {
        expect(sut).toBeTruthy();
    });

    describe('ProductionBuilding', () => {
        it('should getCorrectInterval', () => {
            Injector.gameState.gameSpeed.next(100);
            const config = new GameHandlerConfig({ productionIntervalMs: 5000 });
            expect(config.productionIntervalEffectiveMs).toEqual(5000);

            const configFast = new GameHandlerConfig({ productionIntervalMs: 5000 });
            Injector.gameState.gameSpeed.next(200);
            expect(configFast.productionIntervalEffectiveMs).toEqual(2500);
        });
        it('should getCurrentProductionStock', () => {
            const lumberjackHut = new LumberjackHut();
            lumberjackHut.isProducing.next(true);
            Injector.gameState.doneBuilding(lumberjackHut);
            Injector.gameState.doneBuilding(lumberjackHut);
            // @ts-ignore
            const stock: Partial<Stock> = sut.calculateCurrentProductionStock();
            expect(stock).toEqual({
                wood: 4,
            });
        });

        it('adds production amount in interval', () => {
            const expectedValues = [{}, { wood: 2 }, { wood: 2 }, { wood: 4 }];
            const inventorySpy = jest.spyOn(Injector.gameState.inventory, 'add');
            const lumberjackHut = new LumberjackHut();
            lumberjackHut.isProducing.next(true);

            // @ts-ignore
            sut.handleProductionInterval();

            productionCycleDo.next();
            Injector.gameState.doneBuilding(lumberjackHut);
            productionCycleDo.next();
            productionCycleDo.next();
            Injector.gameState.doneBuilding(lumberjackHut);
            productionCycleDo.next();

            expect(inventorySpy).toHaveBeenCalledWith(expectedValues[0]);
            expect(inventorySpy).toHaveBeenCalledWith(expectedValues[1]);
            expect(inventorySpy).toHaveBeenCalledWith(expectedValues[2]);
            expect(inventorySpy).toHaveBeenCalledWith(expectedValues[3]);
        });
    });
    describe('Victory', () => {
        it('should emit Victory', () => {
            // @ts-ignore
            const stateSpy = jest.spyOn(sut.state.victory, 'next');
            // @ts-ignore
            sut.onNewStock({
                wood: 200,
            });
            expect(stateSpy).toHaveBeenCalledTimes(1);
        });
        it('should not emit Victory', () => {
            // @ts-ignore
            const stateSpy = jest.spyOn(sut.state.victory, 'next');
            // @ts-ignore
            sut.onNewStock({
                wood: 199,
            });
            expect(stateSpy).toHaveBeenCalledTimes(0);
        });
    });
});
