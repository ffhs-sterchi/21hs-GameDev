import { BehaviorSubject, concat, Observable, ReplaySubject, Subject } from 'rxjs';
import { Drawable } from '../../../drawables/drawable';
import { Building } from '../../../drawables/buildings/building';
import { Inventory } from './inventory';
import { Injectable } from '../../../injection/injectable';

/**
 * TODO: describe
 */
export class GameState extends Injectable {
    private readonly _drawables: Drawable[] = [];
    private readonly _drawable: Subject<Drawable> = new Subject<Drawable>();
    private readonly _buildings: Building[] = [];
    private readonly _building: Subject<Building> = new Subject<Building>();

    inventory: Inventory = new Inventory({
        wood: 10,
    });
    readonly gameIsReady = new BehaviorSubject<boolean>(false);
    readonly gameSpeed = new BehaviorSubject<number>(200);
    readonly victory = new Subject<void>();

    constructor() {
        super('gameState', GameState.prototype);
    }

    get drawables(): Drawable[] {
        return this._drawables;
    }

    get drawable(): Observable<Drawable> {
        const subject = new ReplaySubject<Drawable>();
        this._drawables.forEach(subject.next);
        subject.complete();
        return concat(subject, this._drawable);
    }

    get buildings(): Building[] {
        return this._buildings;
    }

    get building(): Observable<Building> {
        return this._building.asObservable();
    }

    addDrawable(...drawables: Drawable[]) {
        drawables.forEach(drawable => {
            this._drawables.push(drawable);
            this._drawable.next(drawable);
        });
    }

    removeBuilding(drawable: Drawable): void {
        const index = this._drawables.indexOf(drawable);
        if (index > -1) {
            this._drawables.splice(index, 1);
        }
        drawable.dispose();
    }

    doneBuilding(building: Building) {
        this._buildings.push(building);
        this._building.next(building);
    }
}
