import { BehaviorSubject, map, Observable } from 'rxjs';

/**
 * TODO: describe
 */
export class Inventory {
    private readonly _stock$: BehaviorSubject<Stock>;

    get stock(): Stock {
        return this._stock$.getValue();
    }

    get stock$(): Observable<Stock> {
        return this._stock$.asObservable();
    }

    constructor(initialStock: Stock) {
        this._stock$ = new BehaviorSubject(initialStock);
    }

    add(stockCountsToAdd: Partial<Stock>) {
        // TODO: implement other resources...
        const newCount = this.stock.wood + (stockCountsToAdd?.wood || 0);
        this._stock$.next({
            ...this.stock,
            wood: newCount > this.stock.wood ? newCount : this.stock.wood,
        });
    }

    remove(stockCountsToRemove: Partial<Stock>) {
        // TODO: implement other resources...
        const newCount = this.stock.wood - stockCountsToRemove.wood;
        this._stock$.next({
            ...this.stock,
            wood: newCount >= 0 ? newCount : 0,
        });
    }

    hasSufficientStock(stockToCheck: Partial<Stock>): boolean {
        return this.isSufficientStock(stockToCheck, this.stock);
    }

    hasSufficientStock$(stockToCheck: Partial<Stock>): Observable<boolean> {
        return this.stock$.pipe(map(stock => this.isSufficientStock(stockToCheck, stock)));
    }

    private isSufficientStock(stockToCheck: Partial<Stock>, actualStock: Stock): boolean {
        return !Object.entries(stockToCheck)
            .map(([stockName, expectedCount]) => {
                const key = stockName as keyof Stock;
                return this.stock[key] >= expectedCount;
            })
            .includes(false);
    }
}

export interface Stock {
    wood: number;
}
