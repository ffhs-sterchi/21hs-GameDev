import { Inventory } from './inventory';
import { bufferCount } from 'rxjs';

describe('Inventory', () => {
    let sut: Inventory;

    beforeEach(() => {
        sut = new Inventory({
            wood: 50,
        });
    });

    describe('Stock', () => {
        it('should be subscribable', done => {
            sut.stock$.subscribe(stock => {
                expect(stock.wood).toEqual(50);
                done();
            });
        });
        it('should emit on update', done => {
            sut.stock$.pipe(bufferCount(2)).subscribe(([initial, updated]) => {
                expect(initial.wood).toEqual(50);
                expect(updated.wood).toEqual(52);
                done();
            });
            sut.add({ wood: 2 });
        });

        describe('StockSufficiency', () => {
            it('is covered', () => {
                expect(
                    sut.hasSufficientStock({
                        wood: 0,
                    }),
                ).toEqual(true);
            });
            it('is not covered', () => {
                expect(
                    sut.hasSufficientStock({
                        wood: 51,
                    }),
                ).toEqual(false);
            });
            it('is covered async', done => {
                sut.hasSufficientStock$({
                    wood: 0,
                }).subscribe(sufficient => {
                    expect(sufficient).toEqual(true);
                    done();
                });
            });
            it('is not covered async', done => {
                sut.hasSufficientStock$({
                    wood: 51,
                }).subscribe(sufficient => {
                    expect(sufficient).toEqual(false);
                    done();
                });
            });
        });

        it('has initial stock', () => {
            expect(sut.stock.wood).toEqual(50);
        });
        it('can be added', () => {
            sut.add({ wood: 5 });
            expect(sut.stock.wood).toEqual(55);
        });
        it('does nothing when adding empty stock', () => {
            sut.add({});
            expect(sut.stock.wood).toEqual(50);
        });
        it('does nothing when adding minus stock', () => {
            sut.add({wood: -2});
            expect(sut.stock.wood).toEqual(50);
        });
        it('does nothing when adding null stock', () => {
            sut.add(null);
            expect(sut.stock.wood).toEqual(50);
        });
        it('can be removed', () => {
            sut.remove({ wood: 5 });
            expect(sut.stock.wood).toEqual(45);
        });
        it('does not go below zero', () => {
            sut.remove({ wood: 51 });
            expect(sut.stock.wood).toEqual(0);
        });
    });
});
