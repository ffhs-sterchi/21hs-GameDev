import { PathWalker, Waypoint } from './path-walker';
import { Mesh, NullEngine, Scene, Vector3 } from '@babylonjs/core';
import { Injector } from '../../injection/dependency-injector';
import { SceneWrapper } from '../../engine/scene-wrapper';
import { GameState } from '../game/state/game-state';

class TestSceneWrapper extends SceneWrapper {
    constructor() {
        super();
        this.scene = new Scene(new NullEngine());
    }

    render() {
        this.scene.onBeforeRenderObservable.notifyObservers(null);
    }

    protected override init() {}
}

describe('Track', () => {
    const testSceneWrapper = new TestSceneWrapper();
    let mesh: Mesh;
    const waypointsAB: Partial<Vector3>[] = [
        { x: 0, z: 0 }, // a
        { x: 1, z: 1 }, // b
    ];
    const waypointsABC: Waypoint[] = [
        ...waypointsAB,
        { x: 2, z: 2 }, // c
    ];

    const setSpeedToMatchDiagonalShift = (ds: number) => {
        const distance = Vector3.Distance(new Vector3(0, 0, 0), new Vector3(ds, 0, ds));
        Injector.gameState.gameSpeed.next((distance / 0.02) * 100);
    };

    beforeEach(() => {
        Injector.provide(testSceneWrapper);
        Injector.provide(new GameState());
        setSpeedToMatchDiagonalShift(0.5);
        mesh = Mesh.CreateBox('dummy', 1, Injector.sceneWrapper.scene);
        mesh.position = Vector3.Zero();
        expectMeshAt(0, 0);
    });

    it('should create', () => {
        const sut = new PathWalker(waypointsAB);
        expect(sut).toBeTruthy();
    });

    it('should set gamespeed to match distance of 0,0 to 0.5,0.5', () => {
        const distance = Vector3.Distance(new Vector3(0, 0, 0), new Vector3(0.5, 0, 0.5));
        const sut = new PathWalker(waypointsAB);
        expect(sut.walkFor()).toBeCloseTo(distance, 5);
    });

    describe('a-b', () => {
        it('should walk', () => {
            expectMeshAt(0, 0);
            const sut = new PathWalker(waypointsAB);
            sut.walk(mesh);

            testSceneWrapper.render();
            expectMeshAt(0.5, 0.5);

            testSceneWrapper.render();
            expectMeshAt(1, 1);

            testSceneWrapper.render();
            expectMeshAt(1, 1);
        });
    });

    describe('a-b-c', () => {
        let sut: PathWalker;
        beforeEach(() => {
            sut = new PathWalker(waypointsABC);
        });

        it('walk 0.5', done => {
            sut.walk(mesh).subscribe(() => {
                done();
            });

            testSceneWrapper.render();
            expectMeshAt(0.5, 0.5);
            testSceneWrapper.render();
            expectMeshAt(1, 1);
            testSceneWrapper.render();
            expectMeshAt(1.5, 1.5);
            testSceneWrapper.render();
            expectMeshAt(2, 2);
            testSceneWrapper.render();
        });
        it('walk 0.7', done => {
            setSpeedToMatchDiagonalShift(0.7);
            sut.walk(mesh).subscribe(() => {
                done();
            });

            testSceneWrapper.render();
            expectMeshAt(0.7, 0.7);
            testSceneWrapper.render();
            expectMeshAt(1.7, 1.7);
            testSceneWrapper.render();
            expectMeshAt(2, 2);
            testSceneWrapper.render();
        });
    });

    const expectMeshAt = (x: number, z: number) => {
        const roundedCoordinates = [mesh.position.x, mesh.position.z].map(
            v => Math.round(v * 10) / 10,
        );
        expect(roundedCoordinates).toEqual([x, z]);
    };
});
