import { Observable, Subject } from 'rxjs';
import { Mesh, Observer, Vector3 } from '@babylonjs/core';
import { Injector } from '../../injection/dependency-injector';

export type Waypoint = Partial<Vector3>;

export class PathWalker {
    private static readonly STEP = 0.02;
    private readonly waypoints: Vector3[];
    private walkers: ((currentWalker: Observer<any>) => void)[] = [];
    private done = new Subject<void>();

    constructor(waypoints: Partial<Vector3>[]) {
        this.waypoints = waypoints.map(point => new Vector3(point.x, point.y, point.z));
    }

    walk(mesh: Mesh): Observable<void> {
        this.walkers = [];
        for (let i = 0; i < this.waypoints.length - 1; i++) {
            const next = this.waypoints[i + 1];
            this.addWalkers(mesh, next);
        }
        this.startNextWalker();
        return this.done;
    }

    private addWalkers(mesh: Mesh, next: Vector3) {
        const stepDistance = (PathWalker.STEP / 100) * Injector.gameState.gameSpeed.getValue();
        this.walkers.push(currentWalkerObserver => {
            mesh.lookAt(next);

            const meshToNextDistance = Vector3.Distance(mesh.position, next);
            const isLastStep = stepDistance > meshToNextDistance;

            const effectiveStep = isLastStep ? meshToNextDistance : stepDistance;
            mesh.movePOV(0, 0, -effectiveStep);

            if (isLastStep) {
                this.startNextWalker(currentWalkerObserver);
            }
        });
    }

    private startNextWalker(currentWalkerObserver: Observer<any> = null) {
        if (currentWalkerObserver) {
            Injector.sceneWrapper.scene.onBeforeRenderObservable.remove(currentWalkerObserver);
        }
        const nextWalker = this.walkers.shift();
        if (nextWalker) {
            const nextWalkerObserver = Injector.sceneWrapper.scene.onBeforeRenderObservable.add(
                () => nextWalker(nextWalkerObserver),
            );
        } else {
            this.done.next();
        }
    }

    walkFor() {
        const step = 0.02;
        return (step / 100) * Injector.gameState.gameSpeed.getValue();
    }
}
