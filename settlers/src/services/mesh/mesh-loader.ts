import { forkJoin, Observable, ReplaySubject } from 'rxjs';
import { Injectable } from '../../injection/injectable';
import { LoadingMesh } from './loading-mesh';
import { Meshes, MeshesKey } from './meshes';

export class MeshLoader extends Injectable {
    public meshes: Meshes = null;

    protected get _meshes() {
        return [
            LoadingMesh.loadAsync('lumberjack', {
                meshFolder: 'dude',
                sceneFilename: 'dude.babylon',
                sourceName: 'him',
            }),
            LoadingMesh.loadAsync('inn', {
                meshFolder: 'inn',
                sceneFilename: 'Medieval_Inn.babylon',
                sourceName: 'Medieval_Inn',
            }),
            LoadingMesh.loadAsync('lumberjackhut', {
                meshFolder: 'lumberjackhut',
                sceneFilename: 'WoodenCabinObj.obj',
                sourceName: null,
            }),
            LoadingMesh.loadAsync('tree', {
                meshFolder: 'trees/low',
                sceneFilename: 'New_Tree_Lod_Packed.obj',
                sourceName: null,
            }),
        ];
    }

    public get isInitialized(): Observable<boolean> {
        return this._isInitialized;
    }

    protected readonly _isInitialized: ReplaySubject<boolean> = new ReplaySubject<boolean>(1);

    constructor() {
        super('meshLoader', MeshLoader.prototype);
        this.initialize();
    }

    protected initialize() {
        forkJoin(this._meshes).subscribe(meshes => {
            this.meshes = meshes.reduce((acc: Meshes, v: LoadingMesh) => {
                acc[v.key as MeshesKey] = v;
                return acc;
            }, {} as Meshes);
            this._isInitialized.next(true);
        });
    }
}

