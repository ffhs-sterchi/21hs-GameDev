import { AssetContainer, Mesh, SceneLoader, Vector3 } from '@babylonjs/core';
import { catchError, from, map, Observable, of } from 'rxjs';
import { MeshesKey } from './meshes';

/**
 * Wrapper to define and keep track of the meshes to load
 * TODO: describe
 */
export class LoadingMesh {
    constructor(readonly key: MeshesKey, readonly container: AssetContainer, readonly mesh: Mesh) {}

    static loadAsync(meshName: MeshesKey, definition: LoadingDefinition): Observable<LoadingMesh> {
        return this.aLoaderFor(definition).pipe(
            map(assetContainer => {
                const mesh = this.getMesh(definition, assetContainer);
                this.initializeDefaultProperties(mesh);
                return new LoadingMesh(meshName, assetContainer, mesh);
            }),
            catchError(err => {
                console.error(err);
                return of(null as LoadingMesh);
            }),
        );
    }

    private static aLoaderFor(definition: LoadingDefinition) {
        return from(
            SceneLoader.LoadAssetContainerAsync(
                `assets/meshes/${definition.meshFolder}/`,
                definition.sceneFilename,
            ),
        );
    }

    private static getMesh(definition: LoadingDefinition, assetContainer: AssetContainer) {
        if (definition.sourceName) {
            return this.getMeshByName(assetContainer, definition);
        } else {
            return this.getAllMeshes(assetContainer);
        }
    }

    private static getAllMeshes(container: AssetContainer) {
        return Mesh.MergeMeshes(container.meshes as Mesh[], true, false, null, false, true);
    }

    private static getMeshByName(container: AssetContainer, definition: LoadingDefinition) {
        const matchingMeshesInContainer = container.meshes.filter(
            m => m.name === definition.sourceName,
        );
        return matchingMeshesInContainer[0] as Mesh;
    }

    private static initializeDefaultProperties(mesh: Mesh): void {
        mesh.isVisible = false;
        mesh.normalizeToUnitCube();
        mesh.position = new Vector3(0, 0, 0);
        const bv = mesh.getHierarchyBoundingVectors();
        mesh.position = new Vector3(0, -bv.min.y, 0);
        mesh.receiveShadows = true;
    }
}

export interface LoadingDefinition {
    meshFolder: string;
    sceneFilename: string;
    sourceName: string;
}
