import { LoadingMesh } from './loading-mesh';

/**
 * All possible meshes
 */
export interface Meshes {
    inn?: LoadingMesh;
    tree?: LoadingMesh;
    lumberjackhut?: LoadingMesh;
    lumberjack?: LoadingMesh;
    warriors?: LoadingMesh;
    house?: LoadingMesh;
}

export type MeshesKey = keyof Meshes;
