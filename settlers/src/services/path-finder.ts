import { Vector2, Vector3 } from '@babylonjs/core';

const dijkstra = require('dijkstrajs/dijkstra.js');
const find_path = dijkstra.find_path;
const DEFAULT_PRECISION = 1;

export class PathFinder {}

export class PathCoordinate {
    constructor(public x: number, public z: number) {}
}

export type NodeId = string | number;
export type AdjacencyMatrix = { [nodeId: NodeId]: AdjacencyEntry };
export type AdjacencyEntry = { [nodeId: NodeId]: number };

/**
 * TODO: describe & refactor
 */
export class PathNodeCollection extends Array<PathNode> {
    public static gridBetween(
        first: Vector3,
        second: Vector3,
        precision: number = DEFAULT_PRECISION,
    ): PathNodeCollection {
        const minCoordinate = Vector3.Minimize(first, second).floor();
        const maxCoordinate = Vector3.Maximize(first, second).floor().addInPlaceFromFloats(1, 1, 1);
        return PathNodeCollection.generateNodesForGrid(minCoordinate, maxCoordinate, precision);
    }

    public static generateNodesForGrid(
        min: PathCoordinate,
        max: PathCoordinate,
        precision: number = DEFAULT_PRECISION,
    ): PathNodeCollection {
        const nodes: PathNodeCollection = new PathNodeCollection(precision);
        let i = 0;
        for (let ix = min.x; ix <= max.x; ix += precision) {
            for (let iz = min.z; iz <= max.z; iz += precision) {
                const pathNode = new PathNode(++i, ix, iz);
                pathNode.generateNeighbours(min, max, precision);
                nodes.push(pathNode);
            }
        }
        return nodes;
    }

    /**
     * Needed because of prototype chain adjustion in es6, see:
     * https://github.com/Microsoft/TypeScript/wiki/FAQ#why-doesnt-extending-built-ins-like-error-array-and-map-work
     */
    constructor(private precision: number = DEFAULT_PRECISION) {
        super();
        Object.setPrototypeOf(this, PathNodeCollection.prototype);
    }

    findShortestPathNear(from: PathCoordinate, to: PathCoordinate): PathCoordinate[] {
        const fromNode = this.getNodeNearCoordinates(from);
        const toNode = this.getNodeNearCoordinates(to);

        let fromId = fromNode.id;
        let toId = toNode.id;

        const matrix = this.toAdjacencyMatrix();
        const path: PathCoordinate[] = [];

        if (fromNode.x !== from.x || fromNode.z !== from.z) {
            fromId = -Infinity;
            matrix[fromId] = { [fromNode.id]: 0 };
            path.push({ x: from.x, z: from.z });
        }

        if (toNode.x !== to.x || toNode.z !== to.z) {
            toId = Infinity;
            matrix[toNode.id] = { [toId]: 0 };
            path.push({ x: to.x, z: to.z });
        }

        const shortestPath = find_path(matrix, fromId, toId)
            .map((id: string) => this.getById(parseInt(id, 10)))
            .filter((n: PathNode) => !!n);

        path.splice(path.length ? 1 : 0, 0, ...shortestPath);
        return path;
    }

    toAdjacencyMatrix(): AdjacencyMatrix {
        return this.reduce((acc, currentNode) => {
            const entries = currentNode.neighbours.reduce((neighbours, neighbourCoordinates) => {
                const neighbourNode: PathNode = this.getByCoordinates(neighbourCoordinates);
                const weight =
                    currentNode.x !== neighbourNode.x && currentNode.z !== neighbourNode.z ? 1 : 3;
                return {
                    ...neighbours,
                    [neighbourNode.id]: weight,
                };
            }, {});
            return {
                ...acc,
                [currentNode.id]: entries,
            };
        }, {} as AdjacencyMatrix);
    }

    getNodeNearCoordinates(coordinates: PathCoordinate) {
        const serachNode = new Vector2(coordinates.x, coordinates.z);
        return this.filter(node => {
            const currentNode = new Vector2(node.x, node.z);
            return serachNode.equalsWithEpsilon(currentNode, this.precision / 2);
        })[0];
    }

    private getByCoordinates(coordinates: number[]): PathNode {
        return this.filter(node => coordinates[0] === node.x).filter(
            node => coordinates[1] === node.z,
        )[0];
    }

    private getById(id: number): PathNode {
        return this.filter(node => node.id === id)[0];
    }
}

/**
 * TODO: describe & refactor
 */
export class PathNode implements PathCoordinate {
    public neighbours: number[][] = [];

    constructor(public id: number, public x: number, public z: number) {}

    public generateNeighbours(
        min: PathCoordinate,
        max: PathCoordinate,
        precision: number = DEFAULT_PRECISION,
    ) {
        const xLower = min.x <= this.x - precision ? this.x - precision : this.x;
        const zLower = min.z <= this.z - precision ? this.z - precision : this.z;
        const xUpper = max.x >= this.x + precision ? this.x + precision : this.x;
        const zUpper = max.z >= this.z + precision ? this.z + precision : this.z;

        for (let x = xLower; x <= xUpper; x += precision) {
            for (let z = zLower; z <= zUpper; z += precision) {
                if (x !== this.x || z !== this.z) this.neighbours.push([x, z]);
            }
        }
    }
}
