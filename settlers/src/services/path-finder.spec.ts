import { AdjacencyMatrix, PathCoordinate, PathNode, PathNodeCollection } from './path-finder';
import { Vector2 } from '@babylonjs/core';

describe('PathFinder', () => {
    function getPathNode(nodes: PathNode[], id: number) {
        return nodes.filter(n => n.id === id)[0];
    }

    describe('Generate Nodes', () => {
        it('should generate nodes', () => {
            const min = { x: 0, z: 0 };
            const max = { x: 2, z: 2 };
            // 3  6  9
            // 2  5  8
            // 1  4  7
            const nodes = PathNodeCollection.generateNodesForGrid(min, max);
            expect(nodes.length).toEqual(9);

            expect(getPathNode(nodes, 1).x).toEqual(0);
            expect(getPathNode(nodes, 1).z).toEqual(0);
            expect(getPathNode(nodes, 1).neighbours.length).toEqual(3);

            expect(getPathNode(nodes, 5).id).toEqual(5);
            expect(getPathNode(nodes, 5).x).toEqual(1);
            expect(getPathNode(nodes, 5).z).toEqual(1);
            expect(getPathNode(nodes, 5).neighbours.length).toEqual(8);
        });

        it('should should take optional precision parameter', () => {
            const min = { x: 0, z: 0 };
            const max = { x: 1, z: 1 };
            const nodes = PathNodeCollection.generateNodesForGrid(min, max, 0.5);
            expect(nodes.length).toEqual(9);

            expect(getPathNode(nodes, 1).x).toEqual(0);
            expect(getPathNode(nodes, 1).z).toEqual(0);
            expect(getPathNode(nodes, 1).neighbours.length).toEqual(3);
            expect(getPathNode(nodes, 1).neighbours).toContainEqual([0, 0.5]);
            expect(getPathNode(nodes, 1).neighbours).toContainEqual([0.5, 0]);
            expect(getPathNode(nodes, 1).neighbours).toContainEqual([0.5, 0.5]);

            expect(getPathNode(nodes, 2).x).toEqual(0);
            expect(getPathNode(nodes, 2).z).toEqual(0.5);

            expect(getPathNode(nodes, 3).x).toEqual(0);
            expect(getPathNode(nodes, 3).z).toEqual(1);

            expect(getPathNode(nodes, 4).x).toEqual(0.5);
            expect(getPathNode(nodes, 4).z).toEqual(0);

            expect(getPathNode(nodes, 5).x).toEqual(0.5);
            expect(getPathNode(nodes, 5).z).toEqual(0.5);
        });
    });
    describe('Generate Nodes Neighbours', () => {
        it('1 x 1', () => {
            const min = { x: 0, z: 0 };
            const max = { x: 0, z: 0 };
            const node = new PathNode(1, 0, 0);
            node.generateNeighbours(min, max);
            expect(node.neighbours.length).toEqual(0);
        });

        it('1 x 2', () => {
            const min = { x: 0, z: 0 };
            const max = { x: 0, z: 1 };
            const node = new PathNode(1, 0, 0);
            node.generateNeighbours(min, max);
            expect(node.neighbours).toContainEqual([0, 1]);
            expect(node.neighbours.length).toEqual(1);
        });
        it('2 x 2', () => {
            const min = { x: 0, z: 0 };
            const max = { x: 1, z: 1 };
            const node = new PathNode(1, 0, 0);

            node.generateNeighbours(min, max);

            expect(node.neighbours).toContainEqual([1, 0]);
            expect(node.neighbours).toContainEqual([0, 1]);
            expect(node.neighbours).toContainEqual([1, 1]);
            expect(node.neighbours.length).toEqual(3);
        });
        it('3 x 3, with precision', () => {
            const min = { x: 0, z: 0 };
            const max = { x: 1, z: 1 };

            const node = new PathNode(1, 0, 0);
            node.generateNeighbours(min, max, 0.5);

            expect(node.neighbours).toContainEqual([0.5, 0]);
            expect(node.neighbours).toContainEqual([0, 0.5]);
            expect(node.neighbours).toContainEqual([0.5, 0.5]);
            expect(node.neighbours.length).toEqual(3);
        });
        it('3 x 3', () => {
            const min = { x: 0, z: 0 };
            const max = { x: 2, z: 2 };
            const node = new PathNode(5, 1, 1);

            node.generateNeighbours(min, max);

            expect(node.neighbours).toContainEqual([0, 0]);
            expect(node.neighbours).toContainEqual([2, 2]);
            expect(node.neighbours).toContainEqual([0, 2]);
            expect(node.neighbours).toContainEqual([2, 0]);
            expect(node.neighbours).toContainEqual([0, 1]);
            expect(node.neighbours).toContainEqual([1, 0]);
            expect(node.neighbours).not.toContain([1, 1]);
            expect(node.neighbours.length).toEqual(8);
        });
        it('3 x 3 on centered origin', () => {
            const min = { x: -1, z: -1 };
            const max = { x: 1, z: 1 };
            const node = new PathNode(5, 0, 0);

            node.generateNeighbours(min, max);

            expect(node.neighbours).not.toContain([0, 0]);
            expect(node.neighbours).toContainEqual([-1, -1]);
            expect(node.neighbours.length).toEqual(8);
        });
        it('5 x 5 on centered origin', () => {
            const min = { x: -2, z: -2 };
            const max = { x: 2, z: 2 };
            const node = new PathNode(5, 0, 0);

            node.generateNeighbours(min, max);

            expect(node.neighbours).not.toContain([0, 0]);
            expect(node.neighbours).toContainEqual([-1, -1]);
            expect(node.neighbours).toContainEqual([1, 1]);
            expect(node.neighbours).toContainEqual([1, 0]);
            expect(node.neighbours).toContainEqual([0, 1]);
            expect(node.neighbours.length).toEqual(8);
        });
    });
    describe('Generate AdjacencyMatrix', () => {
        let matrix: AdjacencyMatrix;
        beforeEach(() => {
            const min = { x: 0, z: 0 };
            const max = { x: 2, z: 2 };
            // 3  6  9
            // 2  5  8
            // 1  4  7
            const nodes: PathNodeCollection = PathNodeCollection.generateNodesForGrid(min, max);
            matrix = nodes.toAdjacencyMatrix();
        });

        it('should add every id as key', () => {
            expect(Object.keys(matrix)).toContain('1');
            expect(Object.keys(matrix)).toContain('5');
            expect(Object.keys(matrix)).toContain('9');
            expect(Object.keys(matrix).length).toEqual(9);
        });

        it('should add diagonal neighbours as entries with weight 1', () => {
            expect(matrix['5']).toHaveProperty('1', 1);
            expect(matrix['5']).toHaveProperty('1', 1);
            expect(matrix['5']).toHaveProperty('9', 1);
        });

        it('should add horizontal neighbours as entries with weight 3', () => {
            expect(matrix['5']).toHaveProperty('4', 3);
            expect(matrix['5']).toHaveProperty('6', 3);
            expect(matrix['5']).toHaveProperty('2', 3);
        });
    });
    describe('Get NodeNearCoordinates', () => {
        it('should get equal', () => {
            const min = { x: -2, z: -2 };
            const max = { x: 4, z: 4 };
            const nodes: PathNodeCollection = PathNodeCollection.generateNodesForGrid(min, max);
            const actual: PathNode = nodes.getNodeNearCoordinates(new PathCoordinate(0, 0));
            expect(actual.x).toEqual(0);
            expect(actual.z).toEqual(0);
        });
        it('should get near', () => {
            const min = { x: -2, z: -2 };
            const max = { x: 4, z: 4 };
            const nodes: PathNodeCollection = PathNodeCollection.generateNodesForGrid(min, max);
            // const pathCoordinate = new PathCoordinate(1.8, 2.3);
            const actual: PathNode = nodes.getNodeNearCoordinates(new PathCoordinate(-0.1, 0.4));
            expect([actual.x, actual.z]).toEqual([0, 0]);
        });
        it('should get near with precision', () => {
            const max = { x: 1, z: 1 };
            const min = { x: 0, z: 0 };
            const nodes: PathNodeCollection = PathNodeCollection.generateNodesForGrid(
                min,
                max,
                0.5,
            );

            const actual: PathNode = nodes.getNodeNearCoordinates(new PathCoordinate(0.26, 0.1));
            expect([actual.x, actual.z]).toEqual([0.5, 0]);
        });
        it('should get near Vector', () => {
            expect(new Vector2(0.1, 0.1).equalsWithEpsilon(new Vector2(0, 0), 0.5)).toEqual(true);
        });
    });
    describe('findPath', () => {
        it('should find the path between two coordinates', function () {
            const min = { x: -2, z: -2 };
            const max = { x: 4, z: 4 };
            const nodes: PathNodeCollection = PathNodeCollection.generateNodesForGrid(min, max);

            const actual = nodes
                .findShortestPathNear(new PathCoordinate(0, 0), new PathCoordinate(2, 2))
                .map(p => [p.x, p.z]);
            expect(actual).toEqual([
                [0, 0],
                [1, 1],
                [2, 2],
            ]);
        });

        it('should find the path between two coordinates, when non exact', function () {
            const min = { x: -2, z: -2 };
            const max = { x: 4, z: 4 };
            const nodes: PathNodeCollection = PathNodeCollection.generateNodesForGrid(min, max);
            const actual = nodes
                .findShortestPathNear(new PathCoordinate(-0.1, 0.4), new PathCoordinate(1.8, 2.3))
                .map(p => [p.x, p.z]);

            expect(actual).toEqual([
                [-0.1, 0.4],
                [0, 0],
                [1, 1],
                [2, 2],
                [1.8, 2.3],
            ]);
        });

        it('should find the path between two coordinates, when non exact with precision', function () {
            const min = { x: 0, z: 0 };
            const max = { x: 1, z: 1 };
            const nodes: PathNodeCollection = PathNodeCollection.generateNodesForGrid(
                min,
                max,
                0.5,
            );

            const actual = nodes
                .findShortestPathNear(new PathCoordinate(0.1, 0.1), new PathCoordinate(1, 1))
                .map(p => [p.x, p.z]);

            expect(actual).toEqual([
                [0.1, 0.1],
                [0, 0],
                [0.5, 0.5],
                [1, 1],
            ]);
        });
    });
});
