import { Message } from './message';
import { Control } from '@babylonjs/gui/2D';
import { Observable } from 'rxjs';

/**
 * TODO: describe
 */
export class Tooltip extends Message {
    constructor() {
        super('', {
            heightInPixels: 50,
            widthInPixels: 150,
        });
        this.hide();
    }

    addControlListener(control: Control, text$?: Observable<string>) {
        control.onPointerEnterObservable.add(() => {
            this.showAndMoveTo(control);
            if (text$) {
                text$.subscribe(text => (this.textBlock.text = text));
            }
        });

        control.onPointerOutObservable.add(() => {
            this.hide();
            this.moveToCenter();
        });
    }
}
