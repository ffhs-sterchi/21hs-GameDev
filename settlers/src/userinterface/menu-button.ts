import { Button, Image } from '@babylonjs/gui/2D';

/**
 * TODO: describe
 */
export class MenuButton extends Button {
    constructor(name: string, imageName: string) {
        super();
        const imageButton = Button.CreateImageOnlyButton(
            `menuButton_${name}`,
            `assets/img/${imageName}`,
        );
        Object.assign(this, imageButton);
        this.image.stretch = Image.STRETCH_UNIFORM;
        this.color = 'transparent';
        this.hoverCursor = 'pointer';
        this.regularGlow();
        this.onPointerEnterObservable.add(() => this.highlightGlow());
        this.onPointerOutObservable.add(() => this.regularGlow());
    }

    public enable(enabled: boolean): void {
        this.alpha = enabled ? 1 : 0.5;
        this.hoverCursor = enabled ? 'pointer' : 'normal';
    }

    private highlightGlow() {
        this.image.shadowColor = '#ffffff88';
        this.image.shadowBlur = 10;
    }

    private regularGlow() {
        this.image.shadowColor = '#ffffff55';
        this.image.shadowBlur = 5;
    }
}
