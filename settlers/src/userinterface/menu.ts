import { AdvancedDynamicTexture, Control, Grid, Rectangle, TextBlock } from '@babylonjs/gui/2D';
import { LumberjackHut } from '../drawables/buildings/lumberjackHut';
import { Building } from '../drawables/buildings/building';
import { Stock } from '../services/game/state/inventory';
import { map, Observable, of } from 'rxjs';
import { Injector } from '../injection/dependency-injector';
import { Message } from './message';
import { Tooltip } from './tooltip';
import { MenuButton } from './menu-button';
import { Injectable } from '../injection/injectable';

/**
 * TODO: describe & refactor
 */
export class Menu extends Injectable {
    private readonly wrapper = AdvancedDynamicTexture.CreateFullscreenUI('UI');
    private readonly card = new Rectangle();
    private readonly grid = new Grid();
    private globalTooltip = new Tooltip();

    constructor() {
        super('userInterface', Menu.prototype);
        this.setupCard();
        this.wrapper.addControl(this.card);
        this.wrapper.addControl(this.globalTooltip);
        this.setupGrid(2, 5);
        this.card.addControl(this.grid);
        this.grid.addControl(this.newBuilderButton(LumberjackHut.prototype), 1, 4);
        this.grid.addControl(this.newDemolishButton(), 1, 0);

        const stockItem: keyof Stock = 'wood';
        this.grid.addControl(Menu.createInventoryTile('wood', 'Wood', 0), 0, 4);

        Injector.gameState.victory.subscribe(() => {
            this.wrapper.addControl(new Message('Victory!'));
            this.card.alpha = 0;
        });
        Injector.gameState.inventory.stock$.subscribe(stock => {
            const inventoryGrid: Grid = this.grid.getChildByName(
                Menu.inventoryCardName(stockItem),
            ) as Grid;
            const inventoryWoodCount: TextBlock = inventoryGrid.getChildByName(
                Menu.inventoryCardCountName(stockItem),
            ) as TextBlock;
            inventoryWoodCount.text = stock.wood.toFixed();
        });
    }

    private static inventoryCardName(stockItem: keyof Stock): string {
        return `inventoryCard_${stockItem}`;
    }

    private static inventoryCardCountName(stockItem: keyof Stock): string {
        return `inventoryCardCount_${stockItem}`;
    }

    private static createInventoryTile(
        stockItem: keyof Stock,
        title: string,
        count: number,
    ): Control {
        const titleBlock = new TextBlock(`inventoryCardTitle_${stockItem}`, title);
        const countBlock = new TextBlock(Menu.inventoryCardCountName(stockItem), count.toFixed());
        titleBlock.textHorizontalAlignment = Control.HORIZONTAL_ALIGNMENT_CENTER;
        titleBlock.color = 'white';
        countBlock.textHorizontalAlignment = Control.HORIZONTAL_ALIGNMENT_CENTER;
        countBlock.color = 'white';
        const stackPanel = new Grid(Menu.inventoryCardName(stockItem));
        stackPanel.addRowDefinition(1 / 3);
        stackPanel.addRowDefinition(1 / 3);
        stackPanel.addRowDefinition(1 / 3);
        stackPanel.addControl(titleBlock, 0);
        stackPanel.addControl(countBlock, 1);
        return stackPanel;
    }

    private inventory = Injector.gameState.inventory;

    private newBuilderButton(buildingType: Building) {
        const button = new MenuButton(
            buildingType.representation.name,
            buildingType.representation.image,
        );

        const stockSufficiency = this.inventory.hasSufficientStock$(buildingType.costs);
        const tooltipText: Observable<string> = stockSufficiency.pipe(
            map(sufficient => (!sufficient ? 'Not enough Resources!\n' : '')),
            map(text => text + `Costs: ${buildingType.costs.wood} Wood`),
        );

        stockSufficiency.subscribe(is => button.enable(is));

        this.globalTooltip.addControlListener(button, tooltipText);

        button.onPointerClickObservable.add(() => {
            if (this.inventory.hasSufficientStock(buildingType.costs)) {
                Injector.constructionHandler.startBuilding(buildingType);
            }
        });
        return button;
    }

    private newDemolishButton() {
        const button = new MenuButton('demolish', 'bomb.png');
        this.globalTooltip.addControlListener(button, of('Demolish a Building'));
        button.onPointerClickObservable.add(() => {
            Injector.constructionHandler.startWrecking();
        });

        return button;
    }

    private setupGrid(rows: number, columns: number) {
        for (let i = 0; i < rows; i++) {
            this.grid.addRowDefinition(1 / rows);
        }
        for (let i = 0; i < columns; i++) {
            this.grid.addColumnDefinition(1 / 5);
        }
        this.grid.paddingTopInPixels = 10;
        this.grid.paddingRightInPixels = 10;
        this.grid.paddingBottomInPixels = 10;
        this.grid.paddingLeftInPixels = 10;
    }

    private setupCard() {
        this.card.widthInPixels = 300;
        this.card.heightInPixels = 150;
        this.card.cornerRadius = 0;
        this.card.left = 0;
        this.card.horizontalAlignment = Control.HORIZONTAL_ALIGNMENT_LEFT;
        this.card.verticalAlignment = Control.VERTICAL_ALIGNMENT_BOTTOM;
        this.card.thickness = 0;
        this.card.background = 'grey';
        this.card.alpha = 0.85;
    }
}
