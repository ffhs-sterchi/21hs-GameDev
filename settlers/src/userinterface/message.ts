import { Control, Rectangle, TextBlock } from '@babylonjs/gui/2D';

export interface MessageOptions {
    widthInPixels: number;
    heightInPixels: number;
}

/**
 * TODO: describe
 */
export class Message extends Rectangle {
    protected readonly textBlock = new TextBlock();
    protected readonly options = {
        widthInPixels: 250,
        heightInPixels: 70,
    };

    constructor(protected text: string = '', options: Partial<MessageOptions> = {}) {
        super();
        Object.entries(options).forEach(
            ([key, value]) => (this.options[key as keyof MessageOptions] = value),
        );
        this.widthInPixels = this.options.widthInPixels;
        this.heightInPixels = this.options.heightInPixels;

        this.initRectangle();
        this.initTextBlock();
    }

    private initTextBlock(): void {
        this.textBlock.text = this.text;
        this.textBlock.color = 'white';
        this.textBlock.fontSize = 14;
        this.textBlock.textWrapping = true;
        this.textBlock.textVerticalAlignment = Control.VERTICAL_ALIGNMENT_CENTER;
        this.addControl(this.textBlock);
        this.textBlock.alpha = 1 / this.alpha;
    }

    private initRectangle(): void {
        this.background = 'grey';
        this.alpha = 0.85;
        this.cornerRadius = 5;
        this.left = 0;
        this.top = 0;
        this.zIndex = 50;
    }

    protected showAndMoveTo(control: Control): void {
        this.show();
        this.left = control.centerX - this.centerX + control.widthInPixels + 20;
        this.top = control.centerY - this.centerY - control.heightInPixels + 20;
    }

    protected moveToCenter() {
        this.left = 0;
        this.top = 0;
    }

    public show() {
        this.alpha = 1;
    }

    public hide() {
        this.alpha = 0;
    }
}
