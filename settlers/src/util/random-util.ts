export class RandomUtil {
    public static randomNumberBetween(min: number, max: number): number {
        return Math.random() * (max - min) + min;
    }

    public static smallRandomOffset(): number {
        return (Math.random() - 0.5) / 2;
    }

    public static shuffleArray(array: any[]): void {
        for (let i = array.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [array[i], array[j]] = [array[j], array[i]];
        }
    }

    public static gridCoordinates(width: number): number[][] {
        const halfWidth = width / 2;
        const positions: number[][] = [];
        for (let x = -halfWidth; x < halfWidth; x++) {
            for (let z = -halfWidth; z < halfWidth; z++) {
                const offsetX = RandomUtil.smallRandomOffset();
                const offsetZ = RandomUtil.smallRandomOffset();
                positions.push([x + offsetX, z + offsetZ]);
            }
        }
        RandomUtil.shuffleArray(positions);
        return positions;
    }
}
