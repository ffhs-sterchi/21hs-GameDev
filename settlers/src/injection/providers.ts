import { GameState } from '../services/game/state/game-state';
import { GameHandler } from '../services/game/game-handler';
import { MeshLoader } from '../services/mesh/mesh-loader';
import { SceneWrapper } from '../engine/scene-wrapper';
import { WorldService } from '../services/world-service';
import { Menu } from '../userinterface/menu';
import { TestMediator } from '../services/test-mediator';
import { LocationHandler } from '../services/location-handler';
import { DrawableFactory } from '../services/game/drawable-factory';
import { ConstructionHandler } from '../services/game/construction-handler';

/**
 * TODO: describe
 */
export abstract class Providers {
    gameState: GameState;
    gameHandler: GameHandler;
    meshLoader: MeshLoader;
    sceneWrapper: SceneWrapper;
    worldService: WorldService;
    userInterface: Menu;
    testMediator: TestMediator;
    locationHandler: LocationHandler;
    drawableFactory: DrawableFactory;
    constructionHandler: ConstructionHandler;
}

export type ProviderName = keyof Providers;
