import { ProviderName } from './providers';

/**
 * TODO: describe
 */
export abstract class Injectable {
    protected constructor(
        public readonly name: ProviderName,
        public readonly prototype: Injectable,
        public readonly dependsOn: ProviderName[] = [],
    ) {}
}
