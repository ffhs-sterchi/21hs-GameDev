import { Providers } from './providers';
import { Injectable } from './injectable';

/**
 * TODO: describe
 */
export class DependencyInjector extends Providers {
    constructor(injections: Partial<DependencyInjector>) {
        super();
        Object.assign(this, injections);
    }

    public provide<T extends Injectable>(provider: T): T {
        this[provider.name] = provider as any;
        return provider;
    }
}

export const Injector = new DependencyInjector({});
