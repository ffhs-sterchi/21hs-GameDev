import { Color3, GroundMesh, Mesh, MeshBuilder, StandardMaterial, Texture } from "@babylonjs/core";

/**
 * TODO: describe
 */
export class Landscape {
    playableGround: GroundMesh;
    landscapeGround: GroundMesh;

    constructor() {
        this.initLandscapeGround();
        this.initPlayableGround();
    }

    private initLandscapeGround() {
        this.landscapeGround = Mesh.CreateGroundFromHeightMap(
            "landscapeGround",
            "assets/textures/heightMap.jpg",
            300,
            300,
            100,
            0,
            50,
            null,
            false
        );

        this.landscapeGround.material = Landscape.grassMaterial(4);
        this.landscapeGround.position.y = -0.21;
        this.landscapeGround.receiveShadows = true;
    }

    private initPlayableGround() {
        this.playableGround = MeshBuilder.CreateGround("playableGround", {
            height: 50,
            width: 50,
        }) as GroundMesh;
        //this.playableGround.position.y = 0;
        this.playableGround.material = Landscape.grassMaterial(4);
        this.playableGround.receiveShadows = true;
    }

    private static grassMaterial(repetition: number) {
        const texture = new Texture("assets/textures/grass-sharp.bmp", null);
        texture.uScale = repetition;
        texture.vScale = repetition;

        const groundMat = new StandardMaterial(`grass-${repetition}`, null);
        groundMat.diffuseTexture = texture;
        groundMat.diffuseColor = Color3.FromHexString("#6f942d");
        groundMat.specularColor = Color3.FromHexString("#000");
        return groundMat;
    }
}
