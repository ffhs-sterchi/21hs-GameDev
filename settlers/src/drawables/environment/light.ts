import { Color3, DirectionalLight, HemisphericLight, ShadowGenerator, Vector3 } from "@babylonjs/core";
import { Drawable } from "../drawable";
import { Injector } from '../../injection/dependency-injector';
import { Landscape } from './landscape';

/**
 * TODO: describe
 */
export class Light {
    private shadowGenerator: ShadowGenerator;
    private readonly state = Injector.gameState;

    constructor(private ground: Landscape) {
        this.createSunLight();
        this.shadowGenerator = this.initShadowGenerator();
        this.state.drawable.subscribe((drawable) => {
            this.addShadowCaster(drawable);
        });
    }

    private initShadowGenerator() {
        const shadowGenerator = new ShadowGenerator(512, this.createShadowLight());
        shadowGenerator.transparencyShadow = true; // needed for the trees
        shadowGenerator.useBlurExponentialShadowMap = true; // blur
        return shadowGenerator;
    }

    private createSunLight(): HemisphericLight {
        let hemisphericLight = new HemisphericLight("sunLight", new Vector3(1, 1, 0), null);
        hemisphericLight.excludedMeshes = [
            this.ground.playableGround,
            this.ground.landscapeGround,
        ];
        hemisphericLight.groundColor = Color3.FromInts(164, 164, 164);
        hemisphericLight.intensity = 0.9;
        return hemisphericLight;
    }

    private createShadowLight() {
        let hemisphericLight = new HemisphericLight("shadowLightSun", new Vector3(1, 1, 0), null);
        hemisphericLight.intensity = 0.5;
        hemisphericLight.includedOnlyMeshes = [
            this.ground.playableGround,
            this.ground.landscapeGround,
        ];
        const light = new DirectionalLight("shadowLight", new Vector3(-1, -2, -1), Injector.sceneWrapper.scene);
        light.position = new Vector3(20, 40, 20);
        light.intensity = 1.5;
        light.includedOnlyMeshes = [
            this.ground.playableGround,
            this.ground.landscapeGround,
        ];
        return light;
    }

    private addShadowCaster(drawable: Drawable) {
        if (!drawable) {
            return;
        }
        drawable.mesh.receiveShadows = true;
        this.shadowGenerator.addShadowCaster(drawable.mesh);
    }
}
