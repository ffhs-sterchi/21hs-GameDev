import { Color3, CubeTexture, MeshBuilder, StandardMaterial, Texture } from "@babylonjs/core";

export class Sky {
    constructor() {
        // TODO: rotate skybox slowly
        const skybox = MeshBuilder.CreateBox("skyBox", { size: 300 }, null);
        const skyboxMaterial = new StandardMaterial("skyBox", null);
        skyboxMaterial.backFaceCulling = false;
        skyboxMaterial.reflectionTexture = new CubeTexture("https://playground.babylonjs.com/textures/skybox", null);
        skyboxMaterial.reflectionTexture.coordinatesMode = Texture.SKYBOX_MODE;
        skyboxMaterial.diffuseColor = new Color3(0, 0, 0);
        skyboxMaterial.specularColor = new Color3(0, 0, 0);
        skybox.material = skyboxMaterial;
    }
}
