import { Drawable } from './drawable';
import { RandomUtil } from '../util/random-util';
import { Injector } from '../injection/dependency-injector';
import { Vector2 } from '@babylonjs/core';

/**
 * TODO: refactor
 */
export class Tree extends Drawable {
    constructor(x: number, z: number) {
        const size = RandomUtil.randomNumberBetween(1.2, 1.6);
        super(Injector.meshLoader.meshes.tree.mesh, x, z, size);

        // TODO: does not belong here...
        Injector.meshLoader.meshes.tree.container.textures.forEach(tex => {
            tex.level = 1.5;
            tex.updateSamplingMode(7);
        });
    }

    static forest(x: number, z: number, size: number, density: number = 1) {
        const treeCount = Math.ceil(size * size * density);
        return RandomUtil.gridCoordinates(size)
            .slice(0, treeCount - 1)
            .map(([xx, zz]) => new Vector2(xx + x, zz + z));
    }
}
