import { Drawable } from './drawable';
import { Injector } from '../injection/dependency-injector';

export class Settler extends Drawable {
    constructor(x: number = -0.2, z: number = -0.8, size = 0.35) {
        super(Injector.meshLoader.meshes.lumberjack.mesh, x, z, size);
        this.rotate(180);
        this.mesh.metadata = {};
    }
}
