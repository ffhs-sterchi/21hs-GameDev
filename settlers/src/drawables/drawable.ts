import { Mesh, Vector3 } from '@babylonjs/core';
import { Injector } from '../injection/dependency-injector';

/**
 * TODO: describe
 */
export abstract class Drawable {
    public static degToRad = (degrees: number) => degrees * (Math.PI / 180);
    private static elementNo = 0;
    readonly mesh: Mesh;

    protected constructor(baseMesh: Mesh, x: number = 0, z: number = 0, size = 1) {
        this.mesh = baseMesh.clone(baseMesh.name + Drawable.elementNo++);
        this.mesh.isVisible = true;
        this.setSize(size);
        this.setPosition(x, z);
        Injector.gameState.addDrawable(this);
    }

    public dispose(): void {
        this.mesh.dispose();
    }

    protected rotate(degree: number): void {
        this.mesh.rotate(Vector3.Up(), Drawable.degToRad(degree));
    }

    private setSize(size: number) {
        this.mesh.scaling = this.mesh.scaling.scale(size);
        this.setPosition();
    }

    private setPosition(x: number = this.mesh.position.x, z: number = this.mesh.position.z) {
        const bounding = this.mesh.getHierarchyBoundingVectors();
        this.mesh.position = new Vector3(x, this.mesh.position.y - bounding.min.y, z);
    }
}
