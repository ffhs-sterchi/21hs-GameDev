import { Building, BuildingRepresentation } from './building';
import { Stock } from '../../services/game/state/inventory';
import { Injector } from '../../injection/dependency-injector';

export class Headquarter extends Building {
    constructor() {
        super(Injector.meshLoader.meshes.inn.mesh, 0, 0, 2);
    }

    get representation(): BuildingRepresentation {
        return {
            image: 'wood-house.png',
            name: 'Hauptquartier',
        };
    }

    get costs(): Partial<Stock> {
        return {};
    }

    get produces(): (keyof Stock)[] {
        return [];
    }
}
