import { Drawable } from '../drawable';
import { Mesh } from '@babylonjs/core';
import { Settler } from '../settler';
import { PointerInfo } from '@babylonjs/core/Events/pointerEvents';
import { Stock } from '../../services/game/state/inventory';
import { BehaviorSubject } from 'rxjs';

/**
 * TODO: describe
 */
export abstract class Building extends Drawable {
    /**
     * Returns the picked building
     * @param pointerInfo
     */
    public static at(pointerInfo: PointerInfo): Building | null {
        return pointerInfo.pickInfo.pickedMesh?.metadata?.building;
    }

    public settler: Settler;
    public isProducing = new BehaviorSubject(false);

    public abstract get representation(): BuildingRepresentation;

    public abstract get costs(): Partial<Stock>;

    public abstract get produces(): (keyof Stock)[];

    protected constructor(baseMesh: Mesh, x: number = 0, z: number = 0, size = 1) {
        super(baseMesh, x, z, size);
        this.mesh.isPickable = true;
        this.mesh.metadata = {
            building: this,
            ...this.getRepresentation(),
        };
    }

    public hasName(name: BuildingName): boolean {
        return this.representation.name === name;
    }

    override dispose() {
        super.dispose();
        this.settler?.mesh.dispose();
    }

    private getRepresentation() {
        return this.representation;
    }
}

export type BuildingName = 'Hauptquartier' | 'Holzfällerhütte';

export interface BuildingRepresentation {
    image: string;
    name: BuildingName;
}
