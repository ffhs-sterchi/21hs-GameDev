import { Building, BuildingRepresentation } from './building';
import { Stock } from '../../services/game/state/inventory';
import { Injector } from '../../injection/dependency-injector';

export class LumberjackHut extends Building {
    constructor() {
        super(Injector.meshLoader.meshes.lumberjackhut.mesh, 3, -3, 1);
        this.rotate(180);
    }

    get representation(): BuildingRepresentation {
        return {
            image: 'log.png',
            name: 'Holzfällerhütte',
        };
    }

    get costs(): Partial<Stock> {
        return {
            wood: 5,
        };
    }

    get produces(): (keyof Stock)[] {
        return ['wood'];
    }
}
